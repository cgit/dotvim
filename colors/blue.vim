" local syntax file - set colors on a per-machine basis:
" vim: tw=0 ts=4 sw=4
" Vim color file inspi01 from pablo
" Maintainer:	Hugues Hiegel <hugues@hiegel.fr>
" Last Change:	2008 July 04

hi clear
set background=dark
if exists("syntax_on")
  syntax reset
endif
let g:colors_name = "blue"

if has("gui_running") || &t_Co >= 255

	hi! CursorLine		ctermbg=236 cterm=none
	hi! CursorColumn	ctermbg=236 cterm=none

	hi! LineNr          term=bold,reverse cterm=bold ctermfg=236
	hi! qfLineNr          term=bold,reverse cterm=bold ctermfg=238

	hi!			Comment			ctermfg=240					cterm=bold
	hi!			ColorColumn		ctermfg=234	ctermbg=none
	hi!			Constant		ctermfg=27					cterm=bold
	hi!			Number			ctermfg=27	ctermbg=none	cterm=bold
	hi!			String			ctermfg=39	ctermbg=24		cterm=none
	hi!			Identifier		ctermfg=33					cterm=bold
	"hi!	link	Keyword Identifier
	hi!			Function		ctermfg=62					cterm=bold
	hi!			Statement		ctermfg=133					cterm=bold
	hi!	link	cStatement Statement
	hi!			Conditional		ctermfg=98					cterm=none
	hi!			cConditional	ctermfg=171					cterm=bold
	hi!			Label			ctermfg=04					cterm=bold
	hi!	link	cLabel Label
	hi!			PreProc			ctermfg=31	ctermbg=236		cterm=none
	hi!			Macro			ctermfg=31	ctermbg=none	cterm=none
	hi!			Type			ctermfg=69					cterm=bold
	hi!			cType			ctermfg=12					cterm=none
	hi!			Special			ctermfg=24	ctermbg=none	cterm=none
	hi!			cSpecial		ctermfg=31	ctermbg=24		cterm=none
	hi!			cFormat			ctermfg=04	ctermbg=24		cterm=bold
	hi!			Error	term=reverse cterm=bold ctermbg=none ctermfg=226
	hi!	link	ErrorMsg Error
	hi!			MoreMsg                              ctermbg=none ctermfg=239
	hi!			Question                             ctermbg=none ctermfg=209
	hi!			Todo			ctermfg=220		ctermbg=28	cterm=bold
	hi!			Directory		ctermfg=02
	"hi			Normal
	hi!			Search		ctermfg=190	ctermbg=none	cterm=bold
	hi!			IncSearch	ctermfg=190	ctermbg=35		cterm=bold
	hi!			Title		ctermfg=03					cterm=bold
	hi!			Column71 ctermfg=70
	hi!			Column80 ctermfg=82

	hi!	link	vimHiCtermFgBg	cType
	hi!	link	vimHiCterm		cType
	hi!	link	vimHiTerm		cType
	hi!			vimGroup		ctermfg=39	ctermbg=none	cterm=bold
	hi!			vimHiGroup		ctermfg=31	ctermbg=none	cterm=bold

	"mails
	hi! link mailHeaderKey   Type
	hi! link mailSubject     LineNr
	hi! link mailHeaderEmail mailEmail
	hi! link mailURL         String
	hi! link mailEmail       Special
	hi! link mailQuoteExp1   mailQuoted1
	hi! link mailQuoteExp2   mailQuoted2
	hi! link mailQuoteExp3   mailQuoted3
	hi! link mailQuoteExp4   mailQuoted4
	hi! link mailQuoteExp5   mailQuoted5
	hi! link mailQuoteExp6   mailQuoted6
	hi! link mailHeader      Statement
	hi! link mailSignature   PreProc
	hi! mailQuoted1 ctermfg=249
	hi! mailQuoted2 ctermfg=245
	hi! mailQuoted3 ctermfg=240
	hi! mailQuoted4 ctermfg=237
	hi! mailQuoted5 ctermfg=234
	hi! mailQuoted6 ctermfg=232

	hi! Visual		ctermfg=none	ctermbg=00	cterm=none

	hi! MatchParen	ctermfg=03	ctermbg=none	cterm=bold

	hi! DiffAdd		term=bold			ctermfg=none	cterm=none	ctermbg=20
	hi! DiffChange	term=bold			ctermfg=none	cterm=none	ctermbg=none
	hi! DiffDelete	term=reverse		ctermfg=18		cterm=none	ctermbg=none
	hi! DiffText	term=none			ctermfg=none	cterm=none	ctermbg=20

	hi! StatusLineCMD		term=reverse,bold		ctermfg=12	ctermbg=none		cterm=bold
	hi! StatusLineINS		term=reverse,bold		ctermfg=46	ctermbg=none		cterm=bold
	hi! link StatusLine StatusLineCMD
	au InsertLeave * hi! link StatusLine StatusLineCMD
	au InsertEnter * hi! link StatusLine StatusLineINS
	hi! User1IN				term=reverse,underline	ctermfg=68	ctermbg=none		cterm=none
	hi! User1OUT			term=reverse,underline	ctermfg=239	ctermbg=none		cterm=none
    " This is tricky, but is needed for new windows...
    hi! User1				term=reverse,underline	ctermfg=239	ctermbg=none		cterm=none
	au BufEnter * hi! link User1 User1IN
	au BufLeave * hi! link User1 User1IN
	hi! User1CMD			term=reverse,underline	ctermfg=68	ctermbg=none		cterm=none
	hi! User1INS			term=reverse,underline	ctermfg=40	ctermbg=none		cterm=bold
	au InsertLeave * hi! link User1 User1CMD
	au InsertEnter * hi! link User1 User1INS
	hi! User2			term=bold				ctermfg=03	ctermbg=none		cterm=bold
	hi! User3			term=bold				ctermfg=236	ctermbg=00			cterm=bold
	hi! User4			term=bold				ctermfg=88	ctermbg=none		cterm=none
	hi! StatusLineNC	term=reverse			ctermfg=240	ctermbg=none		cterm=bold

	hi! VertSplit			term=reverse			ctermfg=08	ctermbg=none	cterm=none
	hi! Folded				term=reverse			ctermfg=00	ctermbg=none	cterm=bold
	hi! link FoldColumn Folded
	hi! LineNR				term=reverse,bold	ctermbg=none ctermfg=236 cterm=bold
	hi! CursorLineNR				term=reverse,bold	ctermbg=none ctermfg=24 cterm=none

	hi!       TabLine		term=underline cterm=none,underline	ctermbg=none ctermfg=00
	hi! link  TabLineFill	TabLine
	hi!       TabLineSel	term=bold							ctermbg=236  ctermfg=68 cterm=none

	hi! SpellBad        term=reverse cterm=bold ctermbg=52 ctermfg=171
	hi! SpellCap        term=reverse cterm=none ctermbg=none ctermfg=04
	hi! SpellRare       term=reverse cterm=bold ctermbg=171 ctermfg=68
	hi! SpellLocal      term=underline cterm=bold ctermfg=02

	hi! WildMenu        term=reverse cterm=bold ctermbg=00 ctermfg=03

	hi! NonText      ctermfg=0   cterm=bold ctermbg=none
	hi! SpecialKey   ctermfg=235 cterm=none ctermbg=none

	hi! makeCommands ctermfg=86 ctermbg=0

	" TODO
	hi! todoTitle1 ctermfg=03 cterm=bold
	hi! todoTitle2 ctermfg=12 cterm=bold
	hi! todoTitle3 ctermfg=36 cterm=bold
	hi! todoTitle4 ctermfg=10 cterm=bold

	hi! todoTask1    ctermfg=none
	hi! todoTask2    ctermfg=248
	hi! todoTask3    ctermfg=242
	hi! todoTask4    ctermfg=238
	hi! todoTaskDone ctermfg=16
	hi! todoSubDone  ctermfg=16 cterm=standout

	hi! todoImportant cterm=bold

	hi! todoLink     ctermfg=171 ctermbg=88 cterm=underline

	hi! rtTicket     ctermfg=208 ctermbg=none cterm=standout


	hi! todoDiggle   ctermfg=236 ctermbg=none cterm=bold
	hi! todoMark     ctermfg=236 ctermbg=08 cterm=bold

	hi! EasyMotionTargetDefault ctermfg=46 cterm=bold ctermbg=34
	hi! EasyMotionTarget				ctermfg=46 cterm=bold ctermbg=none
	hi! EasyMotionShadeDefault				ctermfg=240

	hi! SignColumn   ctermbg=none

elseif &t_Co >= 88
	hi!			Comment			ctermfg=81					cterm=bold
	hi!			Constant		ctermfg=65					cterm=none
	hi!			Number			ctermfg=31					cterm=bold
	hi!			String			ctermfg=39		ctermbg=00	cterm=none
	hi!			Identifier		ctermfg=04					cterm=bold
	hi!			Function		ctermfg=04					cterm=bold
	hi!			Statement		ctermfg=64					cterm=bold
	hi!	link	cStatement Statement
	hi!			Conditional		ctermfg=64					cterm=none
	hi!			cConditional	ctermfg=64					cterm=none
	hi!			Label			ctermfg=04						cterm=bold
	hi!	link	cLabel Label
	hi!			PreProc			ctermfg=84		ctermbg=80		cterm=none
	hi!	link	Macro PreProc
	hi!			cDefine		ctermfg=85		ctermbg=none	cterm=none
	hi!			Type			ctermfg=12					cterm=bold
	hi!			cType		ctermfg=12					cterm=underline
	hi!			Special		ctermfg=64		ctermbg=80	cterm=none
	hi!			cFormat		ctermfg=04						cterm=bold
	hi!			Error											cterm=bold
	hi!			Todo			ctermfg=20		ctermbg=24	cterm=bold
	hi!			Directory		ctermfg=02
	"hi			Normal
	hi!			Search		ctermfg=03	ctermbg=none	cterm=bold
	hi!			IncSearch	ctermfg=03	ctermbg=20		cterm=bold
	hi!			Title		ctermfg=03					cterm=bold

	hi!		Column71 ctermfg=68
	hi!		Column80 ctermfg=11
	
	"mails
	hi! link mailHeaderKey   Type
	hi! link mailSubject     LineNr
	hi! link mailHeaderEmail mailEmail
	hi! link mailURL         String
	hi! link mailEmail       Special
	hi! link mailQuoteExp1   mailQuoted1
	hi! link mailQuoteExp2   mailQuoted2
	hi! link mailQuoteExp3   mailQuoted3
	hi! link mailQuoteExp4   mailQuoted4
	hi! link mailQuoteExp5   mailQuoted5
	hi! link mailQuoteExp6   mailQuoted6
	hi! link mailHeader      Statement
	hi! link mailSignature   PreProc
	hi! mailQuoted1 ctermfg=88
	hi! mailQuoted2 ctermfg=87
	hi! mailQuoted3 ctermfg=86
	hi! mailQuoted4 ctermfg=85
	hi! mailQuoted5 ctermfg=84
	hi! mailQuoted6 ctermfg=83

	hi! Visual		ctermfg=none	ctermbg=00	cterm=none

	hi! MatchParen	ctermfg=03	ctermbg=none	cterm=bold

	hi! DiffAdd		term=bold			ctermfg=none	cterm=none	ctermbg=black
	hi! DiffChange	term=bold			ctermfg=none	cterm=none	ctermbg=black
	hi! DiffDelete	term=reverse		ctermfg=black	cterm=none	ctermbg=none
	hi! DiffText	term=none			ctermfg=none	cterm=none	ctermbg=48

	hi! StatusLineCMD		term=reverse,bold		ctermfg=64	ctermbg=none		cterm=bold
	hi! StatusLineINS		term=reverse,bold		ctermfg=12	ctermbg=none		cterm=bold
	hi! link StatusLine StatusLineCMD
	au InsertLeave * hi! link StatusLine StatusLineCMD
	au InsertEnter * hi! link StatusLine StatusLineINS
	hi! User1CMD			term=reverse,underline	ctermfg=04	ctermbg=none		cterm=bold
	hi! User1INS			term=reverse,underline	ctermfg=12	ctermbg=none		cterm=bold
	au InsertLeave * hi! link User1 User1CMD
	au InsertEnter * hi! link User1 User1INS
	hi! User2			term=bold				ctermfg=03	ctermbg=none		cterm=bold
	hi! User3			term=bold				ctermfg=80	ctermbg=00			cterm=bold
	hi! User4			term=bold				ctermfg=32	ctermbg=none		cterm=none
	hi! StatusLineNC	term=reverse			ctermfg=30	ctermbg=none		cterm=bold

	hi! VertSplit			term=reverse			ctermfg=08	ctermbg=none	cterm=none
	hi! Folded				term=reverse			ctermfg=00	ctermbg=none	cterm=bold
	hi! link FoldColumn Folded
	hi! LineNR				term=reverse,bold	ctermbg=none ctermfg=80 cterm=bold

	hi!       TabLine		term=underline cterm=none,underline	ctermbg=none ctermfg=00
	hi! link  TabLineFill	TabLine
	hi!       TabLineSel	term=bold							ctermbg=32  ctermfg=64 cterm=bold

	hi! CursorLine		ctermbg=00 cterm=none
	hi! CursorColumn	ctermbg=80 cterm=none

	hi! SpellBad        term=reverse cterm=bold ctermbg=04 ctermfg=04 
	hi! SpellCap        term=reverse cterm=none ctermbg=none ctermfg=04 
	hi! SpellRare       term=reverse cterm=bold ctermbg=64 ctermfg=68
	hi! SpellLocal      term=underline cterm=bold ctermfg=02

	hi! WildMenu        term=reverse cterm=bold ctermbg=00 ctermfg=03

	hi! NonText      ctermfg=00 cterm=bold
	hi! SpecialKey   ctermfg=80 cterm=none

	hi! makeCommands ctermfg=86 ctermbg=0

	" TODO
	hi! todoTitle1 ctermfg=03 cterm=bold
	hi! todoTitle2 ctermfg=12 cterm=bold
	hi! todoTitle3 ctermfg=36 cterm=bold
	hi! todoTitle4 ctermfg=10 cterm=bold

	hi! todoTask1    ctermfg=none
	hi! todoTask2    ctermfg=86
	hi! todoTask3    ctermfg=83
	hi! todoTask4    ctermfg=81
	hi! todoTaskDone ctermfg=16 cterm=none
	hi! todoSubDone  ctermfg=16 cterm=standout

	hi! todoImportant cterm=bold

	hi! todoLink     ctermfg=64 ctermbg=32 cterm=underline

	hi! rtTicket     ctermfg=05 ctermbg=none cterm=standout


	hi! todoDiggle   ctermfg=80 ctermbg=none cterm=bold
	hi! todoMark     ctermfg=80 ctermbg=08 cterm=bold
	hi! todoMarkee   ctermfg=16 ctermbg=08 cterm=bold
endif

color NERD

